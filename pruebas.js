const {Pool} = require('pg');
const { database } = require('pg/lib/defaults');

const conexionBD = {
    user: 'postgres',
    host: '127.0.0.1',
    password: 'pgAdmin',
    database: 'HospitalAndres'
}

const pool = new Pool(conexionBD)

const obtenerPersonas = async () => {
    try {
        console.log(await (await pool.query("select * from person")).rows);
    }catch{
        console.log('Error capturado.');
    }
}

obtenerPersonas();

const registrarPersona = async () => {
    try {
        const text = 'INSERT INTO public.person(id_person, number_identification, full_name, full_last_name, birthdate, sex) VALUES ($1, $2, $3, $4, $5, $6);';
        const values = [2, '1234', 'Andres', 'Muñoz', '10-10-2022','M'];

        const res = await pool.query(text, values);
        console.log(res)
        pool.end();
    } catch (e) {
        console.log(e);
    }
};

registrarPersona()
obtenerPersonas();