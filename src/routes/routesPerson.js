const {Router} = require('express');
const router = Router();

const {getPerson, storePerson, getPersonById, deletePersonById, updatePersonById} = require('../controllers/person.controller')

router.get('/person', getPerson);
router.get('/person/:idPerson', getPersonById);
router.delete('/person/:idPerson', deletePersonById);
router.put('/person/:idPerson', updatePersonById);
router.post('/person', storePerson )

module.exports = router;