const express = require('express');
const app = express();
const port = 3000;

//middlewares
app.use(express.json());//procesar a json lo que envía el cliente
app.use(express.urlencoded({extended: false}));// envia datos de formulario el cliente//recibe solo datos planos, no recursos multimedia

//routes
app.use('/api/',require('./routes/routesPerson'));
// var routesPerson = require('./routes/routesPerson');

app.listen(port);
console.log("corriendo en el puerto " + port);