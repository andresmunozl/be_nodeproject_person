const { Pool } = require('pg')

const configBD = {
    user: 'postgres',
    host: '127.0.0.1',
    password: 'pgAdmin',
    database: 'HospitalAndres'
}
const pool = new Pool(configBD);

const getPerson = async (req, res) => {
    // res.send('person')
    try {
        const response = await pool.query('SELECT * FROM  person');
        // console.log(response.rows);
        if (response.rows.length > 0) {
            res.status(200).json(response.rows);
        }
        else {
            res.status(200).json([{status: "No hay personas registradas por ahora, agrega una persona."}]);
        }
    } catch (error) {
        res.status(500).json(error);
    }
}

const getPersonById = async (req, res) => {
    // res.send(req.params.idPerson)
    try {
        const response = await pool.query('SELECT * FROM  person WHERE id_person = $1', [req.params.idPerson]);
        if (response.rows.length > 0) {
            res.status(200).json(response.rows);
        } else {
            res.status(500).json({status: "no se encontró coincidencias"});
        }
    } catch (error) {
        res.status(500).json(error);
    }
}

const deletePersonById = async (req, res) => {
    // res.send(req.params.idPerson)
    try {
        const response = await pool.query('DELETE FROM person where id_person = $1', [req.params.idPerson]);
        res.status(200).json('Se aliminó la persona ' + req.params.idPerson+ ' exitosamente.');
    } catch (error) {
        res.status(500).json(error);
    }
}

const updatePersonById = async (req, res) => {
    // res.send(req.params.idPerson)
    const { id_person, number_identification, full_name, full_last_name, birthdate, sex } = req.body;
    try {
        const text = 'UPDATE person SET number_identification = $1, full_name = $2, full_last_name = $3, birthdate = $4, sex= $5 WHERE id_person = $6';
        const values = [number_identification, full_name, full_last_name, birthdate, sex,  parseInt(req.params.idPerson)];
        const response = await pool.query(text, values);
        res.status(200).json({status :'Se actualizó la persona ' + req.params.idPerson+ ' exitosamente.'});
    } catch (error) {
        res.status(500).json(error);
    }
}

const storePerson = async (req, res) => {
    // console.log(req.body);//prueba del body enviado por el cliente

    const { id_person, number_identification, full_name, full_last_name, birthdate, sex } = req.body;

    try {
        const text = 'INSERT INTO public.person(number_identification, full_name, full_last_name, birthdate, sex) VALUES ($1, $2, $3, $4, $5);';
        const values = [number_identification, full_name, full_last_name, birthdate, sex];

        const response = await pool.query(text, values);
        res.status(200).json({status : "Persona creada!"});
        // pool.end();
    } catch (error) {

    }
}



module.exports = {
    getPerson, storePerson, getPersonById, deletePersonById, updatePersonById
}